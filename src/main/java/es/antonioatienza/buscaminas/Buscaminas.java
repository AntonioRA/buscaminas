package es.antonioatienza.buscaminas;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.geometry.Rectangle2D;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.stage.Screen;
import javafx.stage.Stage;

/**
 * Clase principal del juego buscaminas
 *
 * @author Antonio Rojo Atienza
 */
public class Buscaminas extends Application {

    @Override
    public void start(Stage stage) {

        Button botonFacil = new Button("Nivel Fácil");
        Button botonMedio = new Button("Nivel Medio");
        Button botonDificil = new Button("Nivel Difícil");
//        TextField texto1 = new TextField();
//        TextField texto2 = new TextField();
//        Button botonDescubrir = new Button("Descubrir");
//        Button botonBandera = new Button("Bandera");
        HBox hBox = new HBox(); 
        Rectangle2D visualBounds = Screen.getPrimary().getVisualBounds();
//        HBox hBox2 = new HBox(); 
        hBox.getChildren().add(botonFacil);
        hBox.getChildren().add(botonMedio);
        hBox.getChildren().add(botonDificil);
//        hBox2.getChildren().add(texto1);
//        hBox2.getChildren().add(texto2);
//        hBox2.getChildren().add(botonDescubrir);
//        hBox2.getChildren().add(botonBandera);
        hBox.setAlignment(Pos.CENTER);
        hBox.setSpacing(20);
        GridPane root = new GridPane();
        // Colocar cada panel en su posición de la rejilla
        root.add(hBox, 0, 0);
//        root.add(hBox2,0,1);
        // Centrado en la ventana
        root.setAlignment(Pos.TOP_CENTER);
        Scene scene = new Scene(root, visualBounds.getWidth(), visualBounds.getHeight()); 
        stage.setTitle("Buscaminas");
        stage.getIcons().add(new Image(Buscaminas.class.getResourceAsStream("/mina.png")));
        stage.setScene(scene);
        stage.show();
//        Rectangle2D primScreenBounds = Screen.getPrimary().getVisualBounds();
//        stage.setX((primScreenBounds.getWidth() - stage.getWidth()) / 2); 
//        stage.setY((primScreenBounds.getHeight() - stage.getHeight()) / 4);

//        botonDescubrir.setOnAction(new EventHandler<ActionEvent>() {            
//            @Override
//            public void handle(ActionEvent event) {
//                int numeroFila = Integer.valueOf(texto1.getText());
//                int numeroColumna = Integer.valueOf(texto2.getText());
//                minas.descubrirCelda(numeroFila, numeroColumna);
//            }
//        });
//        
//        botonBandera.setOnAction(new EventHandler<ActionEvent>() {            
//            @Override
//            public void handle(ActionEvent event) {
//                int posicionFila = Integer.valueOf(texto1.getText());
//                int posicionColumna = Integer.valueOf(texto2.getText());
//                minas.banderaCelda(posicionFila, posicionColumna);
//            }
//        });
            
        botonFacil.setOnAction(new EventHandler<ActionEvent>() {            
            @Override
            public void handle(ActionEvent event) {
                int filas = 4;
                int columnas = 4;
                int minasJuego = 5;
                ModeloJuego minas = new ModeloJuego(filas, columnas, minasJuego);
                TableroGrafico tablero = new TableroGrafico(filas,columnas, minas);
                root.add(tablero, 0, 2);
            }
        });
        
        botonMedio.setOnAction(new EventHandler<ActionEvent>() {            
            @Override
            public void handle(ActionEvent event) {
                int filas = 6;
                int columnas = 6;
                int minasJuego = 10;
                ModeloJuego minas = new ModeloJuego(filas, columnas, minasJuego);
                TableroGrafico tablero = new TableroGrafico(filas,columnas, minas);
                root.add(tablero, 0, 2);
            }
        });
        
        botonDificil.setOnAction(new EventHandler<ActionEvent>() {            
            @Override
            public void handle(ActionEvent event) {
                int filas = 8;
                int columnas = 7;
                int minasJuego = 15;
                ModeloJuego minas = new ModeloJuego(filas, columnas, minasJuego);
                TableroGrafico tablero = new TableroGrafico(filas, columnas, minas);
                root.add(tablero, 0, 2);
            }
        });

    }

}
