/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.antonioatienza.buscaminas;

/**
 * Clase del juego que se encarga de la información que contienen cada una de las 
 * casillas en forma de propiedades
 * 
 * @author Antonio Rojo Atienza
 */
public class Celda {
    private boolean contieneMina;
    private int contadorMinas = 0;
    public char estadoActualCelda; 
    public static final char CELDA_NO_DESCUBIERTA = '1';
    public static final char CELDA_DESCUBIERTA = '2';
    public static final char POSIBLE_MINA = '3';
    
    public Celda() {
        this.contieneMina = false;
        this.contadorMinas = 0;
        this.estadoActualCelda = CELDA_NO_DESCUBIERTA;
    }
    
    public boolean isContieneMina() {
        return contieneMina;
    }
    
    public void setContieneMina(boolean contieneMina) {
        this.contieneMina = contieneMina;
    }

    public int getContadorMinas() {
        return contadorMinas;
    }

    public void setContadorMinas(int contadorMinas) {
        this.contadorMinas = contadorMinas;
    }

    public char getEstadoActualCelda() {
        return estadoActualCelda;
    }

    public void setEstadoActualCelda(char estadoActualCelda) {
        this.estadoActualCelda = estadoActualCelda;
    }
    
}

  
