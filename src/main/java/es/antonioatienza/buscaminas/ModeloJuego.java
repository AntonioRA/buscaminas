/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.antonioatienza.buscaminas;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Clase del juego encargada de la formación del tablero y de la colocación de
 * las distintas minas en dicho tablero.
 *
 * @author Antonio Rojo Atienza
 */
public class ModeloJuego {

    private static final Logger LOGGER = Logger.getLogger(ModeloJuego.class.getName());
    public Celda[][] tableroBuscaminas;
    private int filas;
    private int columnas;
    private int numeroMinas;
    private final int ES_MINA = 9;

    /**
     * Constructor que nos va a indicar el número de filas, de columnas y
     * también el número de minas aparte de otras funciones
     *
     * @param filas Número de filas del tablero
     *
     * @param columnas Número de columnas del tablero
     *
     * @param numeroMinas Número de minas que contendrá el tablero
     *
     */
    public ModeloJuego(int filas, int columnas, int numeroMinas) {
        this.filas = filas;
        this.columnas = columnas;
        this.numeroMinas = numeroMinas;
        LOGGER.setLevel(Level.ALL);
        tableroBuscaminas = new Celda[filas][columnas];
        if (numeroMinas > filas * columnas) {
            numeroMinas = filas * columnas;
            LOGGER.warning("El número de minas es superior al número del tablero (array)");
        }
        //Bucle recorre filas
        for (int f = 0; f < filas; f++) {
            //Bucle recorre columnas 
            for (int c = 0; c < columnas; c++) {
                tableroBuscaminas[f][c] = new Celda();
            }
        }
        int filasMina;
        int columnaMina;
        int contadorPonerMinas = 0;
        //Generamos las minas a colocar en el tablero del juego
        do {
            filasMina = (int) (Math.random() * filas);
            columnaMina = (int) (Math.random() * columnas);
            if (!tableroBuscaminas[filasMina][columnaMina].isContieneMina()) {
                tableroBuscaminas[filasMina][columnaMina].setContieneMina(true);
                LOGGER.log(Level.FINEST, "Se ha generado mina en fila: " + filasMina
                        + " columna: " + columnaMina);
                contadorPonerMinas++;
            } else {
                LOGGER.log(Level.FINEST, "No se ha podido generar mina en fila: " + filasMina
                        + " columna: " + columnaMina);
            }
        } while (contadorPonerMinas < numeroMinas);

        //Recuento de minas que hay alrededor de cada celda
        for (int f = 0; f < filas; f++) {
            for (int c = 0; c < columnas; c++) {
                if (tableroBuscaminas[f][c].isContieneMina()) {
                    tableroBuscaminas[f][c].setContadorMinas(ES_MINA);
                    LOGGER.log(Level.FINE, "ContadorMinas: " + String.valueOf(tableroBuscaminas[f][c].getContadorMinas()));
                } else {
                    int contadorMinasAlrededor = 0;
                    //Recorremos las celdas adyacentes
                    for (int i = f - 1; i <= f + 1; i++) {
                        for (int j = c - 1; j <= c + 1; j++) {
                            //Limitamos las posiciones del tablero
                            if (i > -1 && j > -1 && i < filas && j < columnas) {
                                if (tableroBuscaminas[i][j].isContieneMina()) {
                                    contadorMinasAlrededor++;
                                    LOGGER.log(Level.FINE, "Minas Alrededor: " + String.valueOf(contadorMinasAlrededor));
                                }
                            }
                        }
                    }
                    //Modificamos la propiedad del contador de minas de cada celda
                    tableroBuscaminas[f][c].setContadorMinas(contadorMinasAlrededor);
                    LOGGER.log(Level.FINE, String.valueOf(tableroBuscaminas[f][c].getContadorMinas()));
                }
            }
        }
        LOGGER.log(Level.FINE, this.toString());
    }

    /**
     * Método para realizar el funcionamiento del descubrimiento de las celdas
     *
     * @param filaX Coordenada x de la fila deltablero
     *
     * @param columnaY Coordenada y de la columna del tablero
     *
     * @return Nos retorna si se ha podido descubrir la celda
     *
     */
    public boolean descubrirCelda(int filaX, int columnaY) {
        tableroBuscaminas[filaX][columnaY].setEstadoActualCelda(Celda.CELDA_DESCUBIERTA);
        LOGGER.log(Level.FINEST, String.valueOf(filaX + " " + columnaY));
        if (tableroBuscaminas[filaX][columnaY].isContieneMina()) {
            LOGGER.log(Level.FINE, this.toString());
            return true;
        } else if (tableroBuscaminas[filaX][columnaY].getContadorMinas() > 0) {
            LOGGER.log(Level.FINE, this.toString());
            return false;
        } else {
            for (int i = filaX - 1; i <= filaX + 1; i++) {
                for (int j = columnaY - 1; j <= columnaY + 1; j++) {
                    if (i > -1 && j > -1 && i < filas && j < columnas) {
                        if (tableroBuscaminas[i][j].getEstadoActualCelda() == Celda.CELDA_NO_DESCUBIERTA) {
                            this.descubrirCelda(i, j);
                        }
                    }
                }
            }
        }
        LOGGER.log(Level.FINE, this.toString());
        return false;
    }

    /**
     * Método que nos permite poner las banderas en el juego
     *
     * @param posX Coordenada x de la fila del tablero donde queremos colocar la
     * bandera
     *
     * @param posY Coordenada y de la columna del tablero donde queremos colocar
     * la bandera
     *
     * @return Nos retorna si se ha podido colocar la mina o no
     *
     */
    public boolean banderaCelda(int posX, int posY) {
        if (tableroBuscaminas[posX][posY].getEstadoActualCelda() == Celda.CELDA_DESCUBIERTA) {
            LOGGER.log(Level.FINE, "La celda está descubierta y no se ha podido colocar la bandera");
            LOGGER.log(Level.FINE, this.toString());
            return false;
        }
        if (tableroBuscaminas[posX][posY].getEstadoActualCelda() == Celda.POSIBLE_MINA) {
            LOGGER.log(Level.FINE, "La celda está marcada ya como bandera y no se ha podido colocar la bandera");
            tableroBuscaminas[posX][posY].setEstadoActualCelda(Celda.CELDA_NO_DESCUBIERTA);
            LOGGER.log(Level.FINE, this.toString());
            return false;
        }
        if (tableroBuscaminas[posX][posY].getEstadoActualCelda() == Celda.CELDA_NO_DESCUBIERTA) {
            LOGGER.log(Level.FINE, "La celda está oculta y se ha colocado bandera");
            tableroBuscaminas[posX][posY].setEstadoActualCelda(Celda.POSIBLE_MINA);
            LOGGER.log(Level.FINE, this.toString());
            return true;
        }
        return true;
    }

    /**
     * Método con el cual llevamos un contador de las banderas que se han puesto
     *
     * @return Devuelve el número de banderas colocadas
     */
    public int contadorBanderas() {
        int contadorBanderas = 0;
        for (int f = 0; f < filas; f++) {
            for (int c = 0; c < columnas; c++) {
                if (tableroBuscaminas[f][c].getEstadoActualCelda() == Celda.POSIBLE_MINA) {
                    contadorBanderas++;
                }
            }
        }
        return contadorBanderas;
    }

    public void finPartida() {
        int celdasDescubiertas = 0;
        for (int f = 0; f < filas; f++) {
            for (int c = 0; c < columnas; c++) {
                if (tableroBuscaminas[f][c].getEstadoActualCelda() == Celda.CELDA_DESCUBIERTA) {
                    celdasDescubiertas++;
                }
            }
        }  
        if(celdasDescubiertas + numeroMinas == filas*columnas){
            LOGGER.log(Level.FINEST, "La partida ha finalizado");
        }
    }
    public int contador(int posX, int posY){
       return tableroBuscaminas[posX][posY].getContadorMinas();
    }
    
    @Override
    public String toString() {
        String mensaje = "";
        mensaje += "\nMinas: \n";
        for (int f = 0; f < filas; f++) {
            for (int c = 0; c < columnas; c++) {
                if (tableroBuscaminas[f][c].isContieneMina()) {
                    mensaje += "*";
                    LOGGER.log(Level.FINEST, "Coordenadas fila: " + f + "columna: "
                            + c + "Es mina");
                } else {
                    LOGGER.log(Level.FINEST, "Coordenadas fila: " + f + "columna: "
                            + c + "No hay mina");
                    mensaje += ".";
                }
            }
            mensaje += "\n";
        }
        mensaje += "Minas Counter: \n";
        for (int f = 0; f < filas; f++) {
            for (int c = 0; c < columnas; c++) {
                mensaje += tableroBuscaminas[f][c].getContadorMinas();
            }
            mensaje += "\n";
        }
        mensaje += "Estado Actual: \n";
        for (int f = 0; f < filas; f++) {
            for (int c = 0; c < columnas; c++) {
                mensaje += tableroBuscaminas[f][c].getEstadoActualCelda();
            }
            mensaje += "\n";
        }
        mensaje += "Contador Banderas: ";
        mensaje += contadorBanderas();
        return mensaje;
    }
}
