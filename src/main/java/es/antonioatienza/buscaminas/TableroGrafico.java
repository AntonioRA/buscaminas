/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.antonioatienza.buscaminas;

import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;

/**
 *
 * @author Antonio Rojo Atienza
 */
public class TableroGrafico extends HBox {
    GridPane panel = new GridPane();
//    private final double IMAGE_SIZE = 35;
    Image casilla = new Image(getClass().getResourceAsStream("/casilla.png"));
    Image casillaDescubierta = new Image(getClass().getResourceAsStream("/casillaDescubierta.png"));
    Image mina = new Image(getClass().getResourceAsStream("/mina.png"));
    Image imagen1 = new Image(getClass().getResourceAsStream("/1.png"));
    Image imagen2 = new Image(getClass().getResourceAsStream("/2.png"));
    Image imagen3 = new Image(getClass().getResourceAsStream("/3.png"));
    Image imagen4 = new Image(getClass().getResourceAsStream("/4.png"));
    Image imagen5 = new Image(getClass().getResourceAsStream("/5.png"));
    Image imagen6 = new Image(getClass().getResourceAsStream("/6.png"));
    Image imagen7 = new Image(getClass().getResourceAsStream("/7.png"));
    private ImageView ElementoImagen = null;
//    Celda celda = new Celda();

    public TableroGrafico(int filas, int columnas, ModeloJuego minas) {
        this.getChildren().add(panel);
        this.setAlignment(Pos.CENTER);
        panel.getChildren().clear();
        for (int f = 0; f < filas; f++) {
            for (int c = 0; c < columnas; c++) {
                ElementoImagen = new ImageView(casilla);
                panel.add(ElementoImagen, c, f);
            }
        }
        panel.setOnMouseClicked(new EventHandler<MouseEvent>() {
            public void handle(MouseEvent me) {
                int posX = (int) (me.getX() / 61);
                int posY = (int) (me.getY() / 61);
                if (minas.descubrirCelda(posY, posX) == false) {
                    switch (minas.contador(posY, posX)) {
                        case 0:
                            ElementoImagen = new ImageView(casillaDescubierta);
                            panel.add(ElementoImagen, posX, posY);
                            break;
                        case 1:
                            ElementoImagen = new ImageView(imagen1);
                            panel.add(ElementoImagen, posX, posY);
                            break;
                        case 2:
                            ElementoImagen = new ImageView(imagen2);
                            panel.add(ElementoImagen, posX, posY);
                            break;
                        case 3:
                            ElementoImagen = new ImageView(imagen3);
                            panel.add(ElementoImagen, posX, posY);
                            break;
                        case 4:
                            ElementoImagen = new ImageView(imagen4);
                            panel.add(ElementoImagen, posX, posY);
                            break;
                        case 5:
                            ElementoImagen = new ImageView(imagen5);
                            panel.add(ElementoImagen, posX, posY);
                            break;
                        case 6:
                            ElementoImagen = new ImageView(imagen6);
                            panel.add(ElementoImagen, posX, posY);
                            break;
                        case 7:
                            ElementoImagen = new ImageView(imagen7);
                            panel.add(ElementoImagen, posX, posY);
                            break;
                    }
                } else {
                    ElementoImagen = new ImageView(mina);
                    panel.add(ElementoImagen, posX, posY);
                }
            }
        });
    }

}
